
mcq.compute.anova = function (object, flist=NULL, inter=TRUE) {
	if (priv.mcq.is.protquant.object(object)) {
		if (is.null(flist)) {
			stop(paste("You must choose a factor or a combination of factors. \n Available factors are : ", paste(colnames(object@metadata@metadata),collapse=", "), sep=""))
		}else{
			factorList <- flist
		}
		if (!is.vector(factorList)) {
			factorList <- c(factorList)
		}
		
		if (inter) {
			factorinteraction <- '*'
		}else{
			factorinteraction <- '+'
		}
		
		protval <- object@protval

		newObject <- new ("cmcq.anova", proteins = object@proteins, paramlist= list(), resultanov = data.frame())

		newObject@paramlist$factorList <- factorList
		newObject@paramlist$interactionList <- c()

		if (length(factorList) == 2 && inter) {
			newObject@paramlist$interactionList <- c(paste(factorList, collapse="__"))
		}
		if (length(factorList) == 3 && inter) {
			newObject@paramlist$interactionList <- c(paste(factorList[c(1,2)], collapse="__"),
				paste(factorList[c(1,3)], collapse="__"),
				paste(factorList[c(2,3)], collapse="__"),
				paste(factorList, collapse="__"))
		}
		if (length(factorList) > 3) {
			stop(paste("Too many factors in the 'flist' argument."))
		}

		for (factorName in factorList) {
			if (!factorName %in% colnames(object@metadata@metadata)) {
				stop(paste("The factor ",factorName," is not described in your metadata. valid factors are :\n",
								paste(colnames(object@metadata@metadata) , collapse="|")))
			}
		}

		options(contrasts=c("contr.sum","contr.sum"))
		ni <- protval[,c("msrun", "accession", "q")]
		ni <- merge (ni, object@metadata@metadata, by.x="msrun", by.y="row.names")
		resultmodel <- list()

		matrixModel <- NULL
		model <- NULL

		for (factorName in factorList) {
			test <- table(ni$accession,ni[,factorName])
			count.test <- apply(test,MARGIN=1,function(x)sum(x!=0))
			protko <- names(which(count.test<2))
			protok <- names(which(count.test>=2))
			if (length(protko)>0){
				cat(paste("Info : There is only one level for factor ",factorName," for the protein ", protko," ,so there is no model.\n",sep=""))
				newObject@proteins=object@proteins[rownames(object@proteins) %in% protok,]
			}
			ni <- ni[ni$accession %in% protok,]
			ni[,factorName]=as.factor(ni[,factorName])
		}
		
		tabLevels <- unique((ni$accession))
		nbprot<- length(tabLevels)

	
		for (ip in 1:nbprot){
			myprot <- tabLevels[ip]
			tabcour <- ni[ni$accession==myprot,]

			resultByFactor <- c()
			
			if (class(object) == "cmcq.protq.xic") {
				
				ifactor <- 0
			
				resultmodel[myprot] <-list(aov(as.formula(paste("q~",paste(factorList, collapse=factorinteraction),sep="")), data=tabcour))
					
				for (factorName in factorList) {
					ifactor <- ifactor+1
					resultByFactor[factorName]<-anova(resultmodel[[myprot]])["Pr(>F)"][[1]][ifactor]
				}
				if (sum(is.na(resultByFactor)) == length(factorList)) {
					for (factorName in factorList) {
						resultByFactor[factorName] <- NA
					}
					for (factorName in newObject@paramlist$interactionList) {
						resultByFactor[factorName] <- NA
					}
				}else{
					for (factorName in newObject@paramlist$interactionList) {
						ifactor <- ifactor+1
						resultByFactor[factorName]<-anova(resultmodel[[myprot]])["Pr(>F)"][[1]][ifactor]
					}
				}
				model[[ip]] <- resultmodel[[myprot]]
			
			}else{
			
				ifactor <- 1
				tryCatch({
					tmp <-glm(as.formula(paste("q~",paste(factorList, collapse=factorinteraction),sep="")), data=tabcour,family="poisson")
					resultByFactor["proba.good.model"]<-as.numeric(pchisq(tmp$deviance,tmp$df.residual,lower.tail=FALSE))
					for (factorName in factorList) {
						ifactor <- ifactor+1
						resultByFactor[factorName]<-anova(tmp,test="Chisq")["Pr(>Chi)"][[1]][ifactor]
					}
					for (factorName in newObject@paramlist$interactionList) {
						ifactor <- ifactor+1
						resultByFactor[factorName]<-anova(tmp,test="Chisq")["Pr(>Chi)"][[1]][ifactor]
					}
					model[[ip]] <-tmp 
				}, error=function(e){
					cat("ERROR FOR protein ", myprot, " :",conditionMessage(e), "\n")})
				if (is.null(resultByFactor)) {
					resultByFactor[1:(length(newObject@paramlist$factorList)+length(newObject@paramlist$interactionList)+1)]<-NA
				}
			}

			if (is.null(matrixModel)) {
				matrixModel <- matrix (data=NA, ncol=length(resultByFactor), nrow=length(tabLevels))
			}
			matrixModel[ip,]<-resultByFactor
			if (ip%%10==0) cat(paste(ip," proteins\n"))
		}

		newObject@resultanov <- data.frame(matrixModel)
		rownames(newObject@resultanov) <- tabLevels
		newObject@model <- model
		names(newObject@model) <- tabLevels

		#rename column :
		factorNameList <- c(newObject@paramlist$factorList,newObject@paramlist$interactionList)
		if (class(object) == "cmcq.protq.xic") {
			colnames(newObject@resultanov) <- factorNameList
		}else{
			colnames(newObject@resultanov) <- names (resultByFactor)
		}

		anov <- newObject@resultanov
		
		colnames(anov)[(length(colnames(anov))-length(factorNameList)+1):length(colnames(anov))]<- paste("pval_", colnames(anov)[(length(colnames(anov))-length(factorNameList)+1):length(colnames(anov))], sep="")
		for (factorName in factorNameList) {
				anov[,length(colnames(anov))+1] <- p.adjust(newObject@resultanov[,factorName],method="BH")
				colnames(anov)[length(anov)]=paste("padj_",factorName,sep="")
		}
		newObject@resultanov <- anov 

		return (newObject)

	}else{
		stop(paste("the class \"", class(object),"\" is not taken into account by mcq.compute.anova",sep=""))
	}
}


