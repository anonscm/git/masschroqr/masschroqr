
mcq.plot.tukey <- function (object,  file = NULL, protlist=NULL, qprot, labelprot="accession") {
	if (class(object)=="cmcq.tukey") {
		
		# check arguments
		if(!priv.mcq.is.protquant.object(qprot)){
			stop(paste(class(qprot)," is not an object of class 'cmcq.protq'.\n",sep=""))
		}
		if (is.null(protlist)) {
			protlist <- mcq.get.protein.list(object)
		}
		if(labelprot!="accession"&& labelprot!="description"){
			stop("The 'labelprot' argument must be chosen among  'accession' or 'description'.\n")
		}
		
		cc <- c("red","green4","blue","cyan","wheat1","magenta","yellow","gray","darkorange", "brown", "purple", "yellowgreen","turquoise",  "antiquewhite2", "aquamarine",  "aquamarine4",  "chartreuse3", "chocolate" , "coral", "darkred", "darksalmon",  "darkseagreen1","darkslateblue",  "darkslategray1","darkturquoise" , "darkviolet" , "deeppink", "deeppink4", "goldenrod3", "goldenrod4","burlywood3", "lightgreen")
		palette(cc)
		dev.off() # because palette(cc) open a device !
	
		
		tabprot <- merge(qprot@protval, qprot@metadata@metadata, by.x="msrun", by.y="row.names")
		tabprot <- merge(tabprot, qprot@proteins, by="accession")

		factorTukey <- c(paste(object@paramlist$factorTukey, collapse=":"))
		
		if (length(object@paramlist$factorTukey) == 2 ) {
			tabprot$newcol <- paste(unlist(tabprot[object@paramlist$factorTukey[1]]),unlist(tabprot[object@paramlist$factorTukey[2]]),sep=":")
		}
		if (length(object@paramlist$factorTukey) == 3 ) {
			tabprot$newcol <- paste(unlist(tabprot[object@paramlist$factorTukey[1]]),unlist(tabprot[object@paramlist$factorTukey[2]]),unlist(tabprot[object@paramlist$factorTukey[3]]),sep=":")
		}
		colnames(tabprot)[which(colnames(tabprot)=="newcol")] <- factorTukey
		
		if (is.null(file)){
			par(ask=TRUE)
		}else{
			pdf(file, title=gsub('.pdf$','',file))
		}
		par(oma = rep(2,4))
		par(mfrow=c(2,2))

		for (i in 1:length(names(protlist@proteins))) {
			# data
			tabcour <- tabprot[tabprot$accession==names(protlist@proteins)[i],]
			tabcour <- tabcour[order(tabcour$msrun),]
			myprot <-unique(tabcour$accession)
			# ylab
			if (class(qprot)=="cmcq.protq.spectralcounting") {
				ylab <- "Number of spectra"
			}else if (class(qprot)=="cmcq.protq.peakcounting") {
				ylab <- "Number of chromatographic peaks"
			}else {
				ylab <- "protein log10 abundance"
			}		
			# plot
			bp <- boxplot(as.formula(paste("q~",factorTukey,sep="")),data=tabcour,col=cc,las=2)
			# groups
			if (is.null(object@resulttukey[[myprot]]$letters)){
				mtext("no Tukey",side=3,line=0.5,cex=1,outer=FALSE)
			}else{
				df <- data.frame(factor=as.character(names(object@resulttukey[[myprot]]$letters)),letter=as.character(object@resulttukey[[myprot]]$letters))
				bp$namesbis<-bp$names
				bp$namesbis<-gsub("\\.",":",bp$namesbis)
				tabletter <- match(bp$namesbis, df$factor)
				if(max(str_length(df$letter))>3) {
					mtext(df$letter[tabletter],side=3,las=2,line=0.5,at=seq(ncol(bp$stats)),adj=0.5, cex=1,outer=FALSE) 
				} else {
					mtext(df$letter[tabletter],side=3,line=0.5,at=seq(ncol(bp$stats)),adj=0.5, cex=1,outer=FALSE) 
				}
			}
			# title
			if(labelprot=="accession"){
				title(names(protlist@proteins)[i])
			}else{
				title(protlist@proteins[[i]])
			}
			mtext(ylab,side=2, line=3, adj=0.5, cex=0.9,outer=FALSE)
		}
		
		if (!is.null(file)){
			par(mfrow=c(1,1))
			par(oma = rep(0,4))
			dev.off()
		}
	}else{
		stop(paste("the class \"", class(object),"\" is not taken into account by mcq.plot.tukey",sep=""))
	}
}
