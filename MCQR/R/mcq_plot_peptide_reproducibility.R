
mcq.plot.peptide.reproducibility<- function (object, file=NULL) {
	if (priv.mcq.is.peptidequant.object(object)) {

		nbsample <- length(unique(object@peptides$msrun))
		nbpeptiz <- length(unique(object@peptides$peptiz))

		count <- table(object@peptides$peptiz)
		nbsample <- length(unique(object@peptides$msrun))
		nb.pepz.tot <- length(unique(object@peptides$peptiz))
		nb.prot.tot <- length(unique(object@proteins$protein))
		nb.pepz.lost <- NULL
		nb.prot.lost <- NULL

		df <- melt(count)


		for (i in seq(0,1, by=0.05)){
			nb.pepz.kept <- length(count[count>=floor(nbsample*(1-i))])
			nb.pepz.lost <- c(nb.pepz.lost, nb.pepz.tot-nb.pepz.kept)		
			names.pepz.kept <- names(count[count>=floor(nbsample*(1-i))])
			names.pep.kept <- unique(object@peptides$peptide[object@peptides$peptiz %in% names.pepz.kept])
			nb.prot.kept <- length(unique(object@proteins$protein[object@proteins$peptide %in% names.pep.kept]))
			nb.prot.lost <- c(nb.prot.lost, nb.prot.tot-nb.prot.kept)		
		}
		
		nb.prot.lost.norm <- floor(nb.prot.lost * max(nb.pepz.lost)/max(nb.prot.lost))

		df1 <- data.frame(x=seq(0,100, by=5),y=nb.pepz.lost,z=rep("peptides-mz",length(nb.pepz.lost)))
		df2 <- data.frame(x=seq(0,100, by=5),y=nb.prot.lost.norm,z=rep("proteins",length(nb.prot.lost.norm)))
		df3 <- rbind(df1,df2)
		
		
	
		if (is.null(file)){
			par(ask=FALSE)
		}
		else{
			pdf(file, title=gsub('.pdf$','',file), width=15)
		}

		# plots representation
		
		plot1 <- ggplot(df, aes(x=value)) +
		geom_histogram(binwidth=0.5) +
		theme(panel.background=element_blank(),plot.title = element_text(hjust=0.5)) +
		ggtitle("Distribution of the number\n of samples in which\n a peptide-mz was quantified") +
		scale_x_continuous("Number of samples") +
		scale_y_continuous("Number of peptides-mz")

		plot2 <- ggplot(df3, aes(x=x,y=y,colour=z)) +
		geom_point() +
		geom_line() +
		scale_x_continuous("% of NA tolerated per peptide-mz") +
		scale_y_continuous("Number of peptides-mz lost", sec.axis=sec_axis(~./(max(nb.pepz.lost)/max(nb.prot.lost)),name="Number of proteins lost")) +
		theme(legend.position = "none", axis.title.y = element_text(color = "red"),axis.text.y = element_text(color = "red"), axis.text.y.right = element_text(color = "cyan3"),axis.title.y.right = element_text(color = "cyan3"))
		
		grid.arrange(plot1, plot2, ncol=2, nrow = 1)
		
		if (!is.null(file)){
			dev.off()
		}
	}
	else {
		stop(paste("The class \"", class(object),"\" of the input object is not taken into account by mcq.plot.peptide.reproducibility",sep=""))
	}
}
































