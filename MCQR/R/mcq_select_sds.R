
mcq.select.sds <-function (object, factor=NULL, levels=NULL) {
	
	if (is.null(levels) | is.null(factor) ) {
			stop("Please fill in the 'factor' and 'levels' arguments.\n")
	} else {
			if (!is.vector(levels)) {
				levels <- c(levels)
			}
	}
	
	if (class(object) == "cmcq.pepq.sds"){
			if (factor == "fraction") {
				
				# fraction(s) to select
				keep_idx <- NULL
				for (i in 1:length(levels)){
					for (j in 1:length(object)){
						if(unique(object[[j]]@peptides$group)==levels[i]) {
							keep_idx[i] <- j
						}
					}
				}

				protpep.sds <-list()
				for (k in keep_idx) {
					fraction <- unique(object[[k]]@peptides$group)
					protpep.sds[[fraction]] <- object[[k]]
				}
				newobject <- new("cmcq.pepq.sds",protpep.sds)

				# protein from removed fractions
				remove_idx <- seq(1,length(object))
				remove_idx <- setdiff(remove_idx,keep_idx)

				proteinvector <- object[[remove_idx[i]]]@proteins$protein_description
				names(proteinvector) <- object[[remove_idx[i]]]@proteins$accession
				if (length(remove_idx)> 1) {
					for (i in 2:length(remove_idx)){
						proteinvectortemp <- object[[remove_idx[i]]]@proteins$protein_description
						names(proteinvectortemp) <- object[[remove_idx[i]]]@proteins$accession
						proteinvector <- c(proteinvector,proteinvectortemp) # attention retirer les doublons
					}
				}
				protlist <- new("cmcq.protlist",proteins=proteinvector)
				
				returnvector <- list("protlist"=protlist,"newxic.sds"=newobject)
				return(returnvector)
				
			}else{
				protpep.sds <-list()
				for (i in 1:length(object)){
					fraction <- unique(object[[i]]@peptides$group)
					protpep.sds[[fraction]] <- mcq.select(object[[i]],factor=factor,levels=levels)
				}
				newobject <- new("cmcq.pepq.sds",protpep.sds)
				return (newobject)
			}

	}else {
		stop(paste("The class \"", class(object),"\" is not taken into account by mcq.select.sds.\n",sep=""))
	}
	
}
