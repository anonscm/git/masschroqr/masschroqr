
setMethod('summary', "cmcq.pepq.sds",
		function(object, ...) {
			for (i in seq_along(object)) {
				cat (paste("Fraction : ", unique(object[[i]]@peptides$group),"\n", sep=""));
				summary(object[[i]])
				cat ("\n")
			}
		}
)

setMethod('summary', "cmcq.pepq.masschroq",
		function(object, ...) {
			priv.summary.protpep.object(object)
		}
)


setMethod('summary', "cmcq.pepq.skyline",
		function(object, ...) {
			priv.summary.protpep.object(object)
		}
)

setMethod('summary', "cmcq.pepq.by.track",
		function(object, ...) {
			priv.summary.protpep.object(object)
		}
)

setMethod('summary', "cmcq.protq.xic",
		function(object, ...) {
			priv.summary.protquant.object(object)
		}
)

setMethod('summary', "cmcq.protq.sds",
		function(object, ...) {
			for (i in seq_along(object)) {
				cat (paste("Fraction : ", unique(object[[i]]@metadata@metadata$fraction),"\n", sep=""));
				summary(object[[i]])
				cat ("\n")
			}
		}
)

setMethod('summary', "cmcq.protq.peakcounting",
		function(object, ...) {
			priv.summary.protquant.object(object)
		}
)

setMethod('summary', "cmcq.protq.spectralcounting",
		function(object, ...) {
			priv.summary.protquant.object(object)
		}
)

setMethod('summary', "cmcq.protq.by.track",
		function(object, ...) {
			priv.summary.protquant.object(object)
		}
)


setMethod('summary', "cmcq.metadata",
		function(object, ...) {
			print (summary (object@metadata))
		}
)

setMethod('summary', "cmcq.pca.peptide",
		function(object, ...) {

			cat (paste("number of XICs : ", length(object@peptides$peptiz),"\n", sep=""));
			cat (paste("number of peptides-mz : ", length(unique(object@peptides$peptiz)),"\n", sep=""));
			cat (paste("number of peptides : ", length(unique(object@peptides$peptide)),"\n", sep=""));
			cat (paste("number of samples : ", length(unique(object@peptides$msrun)),"\n", sep=""));
		}
)

setMethod('summary', "cmcq.protlist",
		function(object, ...) {

			cat (paste("number of proteins : ", length(object@proteins),"\n", sep=""));
		}
)

