library("XML")
library("data.table")

mcqSaxHandler <- function() {
	## 'local' store visible to functions defined inside
	## handlerFactory
	mcq <- new.env(parent=emptyenv())
	temp <- new.env(parent=emptyenv())

	temp$observedinlist = list()
	temp$peptidesmh = list()
	## return value -- list of functions
	list(startDocument=function() {
				cat ("reading MassChroQML file\n")
				mcq$samples = list()
				mcq$proteins = list()
				mcq$quantify <- matrix(, nrow=0, ncol=2)
				colnames(mcq$quantify) <- c("group_id","quantification_method_id")
				mcq$peptidesmh2proteins = list()
				mcq$groups = list()
				mcq$peptidesmh = data.frame(id=character(), mh=as.numeric(),
						seq=character(),
						mods=character() ,
						stringsAsFactors=FALSE)
				mcq$observedin = data.frame(peptide=character(), sample=character(),
						scan=as.numeric(),
						z=as.numeric() ,
						stringsAsFactors=FALSE)
#  <result quantify_id="q1">
#            <data id_ref="samp2">
# <quanti_item id="quanti_item0" item_id_ref="pep3457" z="2">
				#<quantification_data mz="1374.6829" rt="2684.5085" max_intensity="76777.234" area="629942.13" rt_begin="2676.2924" rt_end="2700.8912" peptide="pep3475" z="2"/>

				mcq$quantipeptides = data.frame(quanti_item_id=character(),
						quantify_id=character(),
						sample_id=character(),
						peptide_id=character(),
						z=as.numeric(),
						mz=as.numeric(),
						rt=as.numeric(),
						max_intensity=as.numeric(),
						area=as.numeric(),
						rt_begin=as.numeric(),
						rt_end=as.numeric(),
						stringsAsFactors=FALSE)

			},
			endDocument=function() {
				cat ("endDocument begin")
				mcq$peptidesmh$proteins <- mcq$peptidesmh2proteins
				mcq$quantipeptides$peptiz=as.factor(paste(mcq$quantipeptides$peptide_id,mcq$quantipeptides$z,sep='-'))

				mcq$quantipeptides <- transform(mcq$quantipeptides, area = as.numeric(area), rt = as.numeric(rt),
						z= as.numeric(z), mz= as.numeric(mz),max_intensity= as.numeric(max_intensity)
						,rt_begin= as.numeric(rt_begin),rt_end= as.numeric(rt_end))
				mcq$quantipeptides$logarea=log10(mcq$quantipeptides$area)


				mcq$observedin <- transform(mcq$observedin, z= as.numeric(z), scan= as.numeric(scan))

				cat ("endDocument end")
			},
			endElement=function(name) {
				if ( name == "peptide_list" ){
					temp$peptidesmh = rbindlist(temp$peptidesmh,  use.names=FALSE, fill=FALSE)
					mcq$peptidesmh = rbindlist(list(mcq$peptidesmh, temp$peptidesmh),  use.names=FALSE, fill=FALSE)
					row.names(mcq$peptidesmh) <- mcq$peptidesmh$id
					mcq$peptidesmh$id <- NULL
					temp$peptidesmh <- NULL


					mcq$observedin = rbindlist(list(mcq$observedin, as.data.frame(temp$observedin[0:temp$j,], stringsAsFactors=FALSE)),  use.names=FALSE, fill=FALSE)
					temp$observedinlist <- NULL

				}
				if ( name == "results" ){
					mcq$quantipeptides = rbindlist(list(mcq$quantipeptides, as.data.frame(temp$quantipeptides[0:temp$i,], stringsAsFactors=FALSE)),  use.names=FALSE, fill=FALSE)
					row.names(mcq$quantipeptides) = mcq$quantipeptides$quanti_item_id
					mcq$quantipeptides$quanti_item_id <- NULL
					temp$quantipeptides <- NULL

				}
			},
			startElement=function(name, atts, ...) {
				## lexical scope often requires use of <<- rather than <-

				if ( name == "data_file" ){
					#cat (atts['path'])
					mcq$samples[[atts['id']]] = toString(atts['path'])
				}
				if ( name == "protein" ){
					mcq$proteins[[atts['id']]] = toString(atts['desc'])
				}
				if ( name == "peptide_list" ){

					cat ("reading identified peptides\n")
					temp$i = 0
					temp$j = 0
					temp$max = 10000
					temp$observedin <- matrix(c("","","","",""), nrow=10000, ncol=4)
					colnames(temp$observedin) <- c('peptide','sample','scan','z')

				}
				if ( name == "peptide" ){
#mh="1536.8385" seq="EQLLDVSLLDVHR" mods=" " prot_ids="P51.01"
					temp$peptide_id <- toString(atts['id'])
					protList = strsplit(toString(atts['prot_ids']), " ")
					mcq$peptidesmh2proteins[[temp$peptide_id]] = protList

					i = length(temp$peptidesmh)+1
					temp$peptidesmh[[temp$peptide_id]] <- list(id= temp$peptide_id,mh= as.numeric(atts['mh']),
							seq =   toString(atts['seq']), mods=  toString(atts['mods']))
					#print (temp$peptidesmh[i])
					if (i%%1000 == 0) {
						cat (paste(i," peptides\n"))
					}
				}
				if ( name == "results" ){
					temp$i = 0
					temp$max = 10000
					temp$quantipeptides <- matrix(c("","","","",""), nrow=10000, ncol=11)
					colnames(temp$quantipeptides) <- c('quanti_item_id','quantify_id','sample_id','peptide_id','z','mz','rt','max_intensity'
							,'area','rt_begin','rt_end')

					cat ("reading quantification results\n")
				}
				if (name == "group") {
					#<group id="G1" data_ids="samp2 samp3"/>
					sampleList = strsplit(toString(atts['data_ids']), " ")
					mcq$groups[[atts['id']]] = sampleList
				}
				if ( name == "quantify" ){
					# <quantify id="q1" withingroup="G1" quantification_method_id="quant1">
					mcq$quantify <- rbind(mcq$quantify, c("",""))
					i <- nrow(mcq$quantify)
					mcq$quantify[i,'group_id'] <- atts['withingroup']
					mcq$quantify[i,'quantification_method_id'] <- atts['quantification_method_id']
					rownames(mcq$quantify)[i] <- atts['id']
				}
				if ( name == "observed_in" ) {
					#<observed_in data="samp0" scan="9913" z="2"/>
					if (temp$j < temp$max) {
						temp$j <- temp$j +1
						temp$observedin[temp$j,'peptide'] <- temp$peptide_id
						temp$observedin[temp$j,'sample'] <- atts['data']
						temp$observedin[temp$j,'scan'] <- atts['scan']
						temp$observedin[temp$j,'z'] <- atts['z']
					}
					if (temp$j == temp$max) {
						temp$j = 0
						mcq$observedin = rbindlist(list(mcq$observedin, as.data.frame(temp$observedin, stringsAsFactors=FALSE)),  use.names=FALSE, fill=FALSE)
					}
				}
				if ( name == "result" ){
					#  <result quantify_id="q1">
					temp$quantify_id = toString(atts['quantify_id'])
				}
				if ( name == "data" ){
					#            <data id_ref="samp2">
					temp$sample_id = toString(atts['id_ref'])
				}
				if ( name == "quanti_item" ){
					# <quanti_item id="quanti_item0" item_id_ref="pep3457" z="2">
					temp$quanti_item_id = toString(atts['id'])
				}
				if (name == "quantification_data") {
					#<quantification_data mz="1374.6829" rt="2684.5085" max_intensity="76777.234" area="629942.13" rt_begin="2676.2924" rt_end="2700.8912" peptide="pep3475" z="2"/>
					if (temp$i < temp$max) {
						temp$i <- temp$i +1
						temp$quantipeptides[temp$i,'quanti_item_id'] <- temp$quanti_item_id
						temp$quantipeptides[temp$i,'quantify_id'] <- temp$quantify_id
						temp$quantipeptides[temp$i,'sample_id'] <- temp$sample_id
						temp$quantipeptides[temp$i,'peptide_id'] <- atts['peptide']
						temp$quantipeptides[temp$i,'z'] <- atts['z']
						temp$quantipeptides[temp$i,'mz'] <- atts['mz']
						temp$quantipeptides[temp$i,'rt'] <- atts['rt']
						temp$quantipeptides[temp$i,'max_intensity'] <- atts['max_intensity']
						temp$quantipeptides[temp$i,'area'] <- atts['area']
						temp$quantipeptides[temp$i,'rt_begin'] <- atts['rt_begin']
						temp$quantipeptides[temp$i,'rt_end'] <- atts['rt_end']
					}
					if (temp$i == temp$max) {
						temp$i = 0
						mcq$quantipeptides = rbindlist(list(mcq$quantipeptides, as.data.frame(temp$quantipeptides, stringsAsFactors=FALSE)),  use.names=FALSE, fill=FALSE)
						cat (paste(nrow(mcq$quantipeptides)[[1]]," quantification results\n"))
					}

				}
			}, getMcq=function() {
				as.list(mcq)
			})
}

