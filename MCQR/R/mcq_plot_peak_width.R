
mcq.plot.peak.width <- function (object, file=NULL, limit=NULL, binwidth=5, type="histogram", summed = TRUE) {
	if (class(object) == "cmcq.pepq.masschroq") {
		width_peak <- object@peptides[,'rtend'] - object@peptides[,'rtbegin']
		width_peak = as.data.frame(width_peak)
	}else if ((class(object) == "cmcq.pepq.sds") & (summed)) {
		width_peak <- object[[1]]@peptides$rtend - object[[1]]@peptides$rtbegin
		for (i in 2:length(object)) {
			width_peak <-c(width_peak,object[[i]]@peptides$rtend - object[[i]]@peptides$rtbegin)
		}
		width_peak = as.data.frame(width_peak)
	}else if ((class(object) == "cmcq.pepq.sds") & (!summed)){
		list_dataframe = NULL
		for (i in 1:length(data)){
			list_dataframe[[i]] = data[[i]]@peptides
		}
		width_peak = rbindlist(list_dataframe)
		width_peak$peakwidth = width_peak$rtend-width_peak$rtbegin
	}else {
		if (nrow(object@quantipeptides)[[1]] == 0) {
			stop("No quantification results\n")
		}
		width_peak <- object@quantipeptides[,'rt_end'] - object@quantipeptides[,'rt_begin']
		width_peak = as.data.frame(width_peak)
	}

	if (!is.null(file)){
		pdf(file, title=gsub('.pdf$','',file))
	}
	if (is.null(limit)) {
		if (type == "density"){
			if ((class(object) == "cmcq.pepq.sds") & (!summed)){
				p <- ggplot(width_peak, aes(x=peakwidth, color=group))+geom_density(alpha=0.6)
#~ 				hist(width_peak,main="Distribution of peak width",xlab="Width of chromatographic peaks (seconds)", nclass=nclass, border="darkblue")
			}else{
				p<-ggplot(width_peak, aes(x=width_peak, color="red"))+geom_density(alpha=0.6)+ theme(legend.position="none")
			}
		}else if (type == "histogram"){
			if ((class(object) == "cmcq.pepq.sds") & (!summed)){
				p<-ggplot(width_peak, aes(x=peakwidth, color=group, fill=group))+geom_histogram(binwidth = binwidth, alpha=0.6)
#~ 				hist(width_peak,main="Distribution of peak width",xlab="Width of chromatographic peaks (seconds)", nclass=nclass, border="darkblue")
			}else{
				p<-ggplot(width_peak, aes(x=width_peak, color="red", fill="red"))+geom_histogram(binwidth = binwidth, alpha=0.6)+ theme(legend.position="none")
			}
		}
	}else {
		if (type == "density"){
			if ((class(object) == "cmcq.pepq.sds") & (!summed)){
				p<-ggplot(width_peak, aes(x=peakwidth, color=group))+geom_density(alpha=0.6)+xlim(0,limit)
#~ 				hist(width_peak,main="Distribution of peak width",xlab="Width of chromatographic peaks (seconds)", nclass=nclass, border="darkblue")
			}else{
				p<-ggplot(width_peak, aes(x=width_peak, color="red"))+geom_density(alpha=0.6)+xlim(0,limit)+ theme(legend.position="none")
			}
		}else if (type == "histogram"){
			if ((class(object) == "cmcq.pepq.sds") & (!summed)){
				p<-ggplot(width_peak, aes(x=peakwidth, color=group, fill=group))+geom_histogram(binwidth = binwidth, alpha=0.6)+xlim(0,limit)
#~ 				hist(width_peak,main="Distribution of peak width",xlab="Width of chromatographic peaks (seconds)", nclass=nclass, border="darkblue")
			}else{
				p<-ggplot(width_peak, aes(x=width_peak, color="red", fill="red"))+geom_histogram(binwidth = binwidth, alpha=0.6)+xlim(0,limit)+ theme(legend.position="none")
			}
		}
	}
	print(p)
#~ 	mtext(paste("Quantiles: 10% = ", round(quantile(width_peak, 0.1), 1),"; 25% = ",round(summary(width_peak)[2], 1),"; 50% = ",round(summary(width_peak)[3], 1),"; 75% = ", round(summary(width_peak)[5], 1),"; 90% = ", round(quantile(width_peak, 0.9), 1), sep=""),side=3)
	if (!is.null(file)){
		dev.off()
	}

}
