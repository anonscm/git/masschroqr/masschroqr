mcq.compute.pca = function (object, flist=NULL) {
	if (length(colnames(object@metadata@metadata)) == 0) {
		stop("Metadata are required")
	}

	if  ((priv.mcq.is.peptidequant.object(object)) || (priv.mcq.is.protquant.object (object))) {
		if (priv.mcq.is.protquant.object (object)) {
			tp=object@proteins
			rownames(tp) <- tp$accession
		} else {
			tp <- unique(object@peptides[, c("peptide","peptiz")])
			tp$peptide <- as.character(tp$peptide) 
			tp$peptiz <- as.character(tp$peptiz) 
			rownames(tp)=tp$peptiz
		}
		
		tacp= mcq.get.compar(object, flist)
		tacp <- na.omit(tacp)
		
		# cleaning dataframes after na.omit
		pList <- rownames(tacp)	
		
		if (length(rownames(tacp)) < 3) {
			stop(paste("Too few variables (",length(rownames(tacp)),") to perform PCA", sep=""))
		}
		if (length(colnames(tacp)) < 3) {
			stop(paste("Too few individuals (",length(colnames(tacp)),") to perform PCA", sep=""))
		}
		
		acpnf <- length(colnames(tacp))-1
		if (acpnf > 5) {
			acpnf <- 5
		}
		
		tp <- tp[rownames(tp) %in% pList,]
						
		if (priv.mcq.is.protquant.object (object))  {
			xname <- "proteins"
			newObject <- new ("cmcq.pca.protein", tag=tp, metadata = object@metadata, pca = list())
			newObject@pca$dudi <- dudi.pca(t(tacp),center = TRUE, scale = TRUE, scannf = FALSE, nf = acpnf) 
			newObject@pca$factorList <- flist
			newObject@pca$nf <- acpnf
			newObject@pca$qname <- "q"
		}
		else {
			xname <- "peptides-mz"
			newObject <- new ("cmcq.pca.peptide", tag=tp, metadata = object@metadata, pca = list())
			newObject@pca$dudi <- dudi.pca(t(tacp),center = TRUE, scale = TRUE, scannf = FALSE, nf = acpnf) 
			newObject@pca$factorList <- flist
			newObject@pca$nf <- acpnf
			if (is.null(object@peptides$lognorm)) {
				newObject@pca$qname <- "logarea"
			}else{
				newObject@pca$qname <- "lognorm"
			}
		}
		
		if (is.null(flist)) {
			cat (paste("Info : PCA performed using the",dim(newObject@pca$dudi$tab)[2], xname,"quantified in all the", dim(newObject@pca$dudi$tab)[1], "samples.\n")) 
		}else{
			cat (paste("Info : PCA performed using the",dim(newObject@pca$dudi$tab)[2], xname,"quantified in all the", dim(newObject@pca$dudi$tab)[1], "combinations of factors.\n")) 
		}

		return (newObject)
		
} else {
		stop(paste("the class \"", class(mcq),"\" is not taken into account by mcq.compute.pca",sep=""))
	}
}

