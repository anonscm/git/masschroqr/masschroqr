
mcq.plot.intensity.distribution<- function (object, nclass=100, file=NULL) {
	if ((class(object) == "cmcq.pepq.masschroq") || (class(object) == "cmcq.pepq.skyline")) {
		tab.acp<- tapply (object@peptides$logarea, list(object@peptides$peptiz,object@peptides$msrun), FUN=sum)
		nbech=dim(tab.acp)[2] 
		nbpep=dim(tab.acp)[1] 

		if (is.null(file)){
			par(ask=FALSE)
		}
		else{
			pdf(file, title=gsub('.pdf$','',file))
		}
		par(mfrow=c(2,2))
		for (i in 1:nbech) {
			hist(tab.acp[,i], nclass=nclass, main=paste("Intensity distribution for ", colnames(tab.acp)[i], sep=""), xlab="Log10-transformed intensities", cex.main=0.8)
			if (is.null(file)){
				par(ask=TRUE)
			}
		}
		par(mfrow=c(1,1))
		if (!is.null(file)){
			dev.off()
		}
		
	} else if (class(object) == "cmcq.pepq.sds") {
		for (i in 1:length(object)) {
			if (is.null(file)) {
				fraction_file <- file
			}else{
				fraction_file <- paste(unique(object[[i]]@metadata@metadata$fraction),file,sep="_")
			}
			mcq.plot.intensity.distribution(object[[i]],nclass=nclass,file=fraction_file)
		}
	} else {
		stop(paste("The class \"", class(object),"\" of the input object is not taken into account in the function mcq.plot.intensity.distribution.",sep=""))
	}
}


