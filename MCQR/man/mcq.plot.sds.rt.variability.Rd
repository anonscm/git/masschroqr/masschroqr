\name{mcq.plot.sds.rt.variability}

\alias{mcq.plot.sds.rt.variability}

\title{Plotting the Variability of Retention Time}

\description{This function draws the density curves of the standard deviations of the peptides retention times by fraction.}

\usage{mcq.plot.sds.rt.variability(object, limit=NULL, file=NULL)}

\arguments{
	\item{object}{an object of class 'cmcq.pepq.sds'}
	\item{limit}{a numerical value indicating the upper bound of the x-axis}
	\item{file}{a character string indicating the name of the pdf file where to export the density curves (e.g. "mygraph.pdf"). By default, the graph is displayed on the screen}
}

\details{For each fraction, each peptide in the input object, the standard deviation of retention time is computed from the retention times obtained in the injections where the peptide was quantified.}

\value{'mcq.plot.sds.rt.variability' returns a graph that is displayed on the screen when the file argument is NULL or that is exported as a pdf file when the file argument is filled in.}

\references{}

\author{}

\note{}

\seealso{}

\examples{}

\keyword{ ~kwd1 }
\keyword{ ~kwd2 }
