\name{mcq.select.from.protlist}

\alias{mcq.select.from.protlist}

\title{Selection of a subset from differents objects}

\description{This function selects the data corresponding to a list of proteins.}

\usage{mcq.select.from.protlist(object, protlist)}

\arguments{
	\item{object}{an object of class 'cmcq.protlist','cmcq.protq.xic','cmcq.protq.peakcounting','cmcq.protq.spectralcounting','cmcq.pepq.masschroq','cmcq.pepq.skyline' or 'cmcq.anova'}
	\item{protlist}{an object of class 'cmcq.protlist'}
}

\details{To select a subset from a given object, fill the argument protlist with list of protein in an object of class 'cmcq.protlist' }

\value{'mcq.select.from.protlist' returns the input object containing the list of proteins provided in the 'cmcq.protlist' object passed in argument.}

\references{}

\author{}

\note{}

\examples{
# Preliminary step: load the example dataset
	data(strepto)

# Make a cmcq.protlist object containing the description of 6 proteins 
	plist <- mcq.protlist(c("ortho_SCD40A.01, coelicolor (SSPG_03025T0) SCD82.27, rpoC, SCO4655, DNA-directed RNA polymerase beta' chain (fragment) 5081643:5085542 forward MW:144574"
				,"ortho_SCD82.26, coelicolor (SSPG_03026T0) rpoB, SCO4654, DNA-directed RNA polymerase beta chain 5078060:5081545 forward MW:128477"
				,"ortho_tkt, lividans (SSPG_05611T0) SCO6663, transketolase 7400592:7402688 reverse MW:74588"
				,"SSPG_01038T0 | SSPG_01038 | Streptomyces lividans TK24 transketolase (699 aa)"
				,"ortho_SCCB12.05c, coelicolor (SSPG_02399T0) SCO5281, putative 2-oxoglutarate dehydrogenase 5749846:5753664 reverse MW:139177"
				,"ortho_2SC6G5.15, coelicolor (SSPG_02337T0) atpA, SCO5371, ATP synthase alpha chain 5840171:5841760 forward MW:57257"
		))
		
# Launch mcq.select.from.protlist to select the 6 proteins from the object name sc
	sc.selected <- mcq.select.from.protlist(sc, protlist=plist)
	summary(sc)
	summary(sc.selected)
}

