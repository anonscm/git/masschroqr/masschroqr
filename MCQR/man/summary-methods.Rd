\name{summary}

\alias{summary}

\title{Summary of Object Content}

\description{This function provides a summary of the data contained in the input object.}

\usage{summary(object)}

\arguments{
	\item{object}{an object of class 'cmcq.pepq.masschroq', 'cmcq.pepq.skyline', 'cmcq.protq.xic', 'cmcq.protq.peakcounting', 'cmcq.protq.spectralcounting', 'cmcq.metadata', 'cmcq.pca.peptide', 'cmcq.pca.protein', 'cmcq.protlist', 'cmcq.anova', 'cmcq.pepq.sds', 'cmcq.pepq.by.track'}
}

\value{'summary' displays information relative to the content of the object given as input. This information may differ depending on the type of object.}


\examples{
# Preliminary step: load the example dataset
	data(strepto)

# Display a summary of the protPep object
	summary(XICRAW)
}
