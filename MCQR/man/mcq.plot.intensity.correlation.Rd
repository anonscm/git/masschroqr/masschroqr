\name{mcq.plot.intensity.correlation}

\alias{mcq.plot.intensity.correlation}

\title{Plotting Intensities Correlations for Pairs of injections}

\description{This function draws a graph showing the peptide-mz intensities of a given injection versus the peptide-mz intensities of a injection chosen as a reference.}

\usage{mcq.plot.intensity.correlation(object, refSample=NULL, file=NULL)
}

\arguments{
	\item{object}{an object of class 'cmcq.pepq.masschroq', 'cmcq.pepq.skyline' or 'cmcq.pepq.by.track'}
	\item{refSample}{a character string indicating the name of the msrun corresponding to the injection chosen as a reference. See the column 'msrun' in the metadata file}
	\item{file}{a character string indicating the name of the pdf file where to export the histograms (e.g. "mygraph.pdf"). By default, the graphs are displayed on the screen}
}
\details{Log-transformed peptide-mz intensities are used for plotting. The coefficient of correlation is indicated on each graph. The first bisector is shown in red.}

\value{'mcq.plot.intensity.correlation' returns a series of graphs that are displayed on the screen when the file argument is NULL or that are exported as a pdf file when the file argument is filled in. }

\references{}

\author{}

\note{}


\seealso{'cor' and 'cor.test' in the stats package}

\examples{
# Preliminary step: load the example dataset
	data(strepto)

# Launch 'mcq.plot.intensity.correlation'
	mcq.plot.intensity.correlation(xicraw, refSample="msruna1")

}


