\name{mcq.plot.counts.per.injection}

\alias{mcq.plot.counts.per.injection}

\title{Plotting Spectral or Peak Counts}

\description{Depending on the class of the object given as input, this function plots either the total number of chromatographic peaks quantified per injection or the total number of spectra per injection.}

\usage{mcq.plot.counts.per.injection(object, file=NULL)}

\arguments{
    \item{object}{an object of class 'cmcq.pepq.masschroq', 'cmcq.pepq.sds' or 'cmcq.protq.spectralcounting'}
    \item{file}{a character string indicating the name of the pdf file where to export the histogram (e.g. "mygraph.pdf"). By default, the graph is displayed on the screen} 
}

\details{}

\value{'mcq.plot.counts.per.injection' returns a graph that is displayed on the screen when the pdf argument is NULL or that is exported as a pdf file when the pdf argument is filled in.}

\references{}

\author{}

\note{}


\seealso{}

\examples{
# Preliminary step: load the example dataset 
	data(strepto)

# Launch 'mcq.plot.counts.per.injection'
	mcq.plot.counts.per.injection(xicraw)
}
 
