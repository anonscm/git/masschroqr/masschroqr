\name{mcq.plot.peptide.reproducibility}

\alias{mcq.plot.peptide.reproducibility}

\title{Plotting of Peptide Reproducibility}

\description{This function draws graphs showing the reproducibility of the peptides-ms quantified. Highly reproducible peptides-mz are those that are quantified in a high number of injections, i.e. those that have little missing values.}

\usage{mcq.plot.peptide.reproducibility(object, file=NULL)}

\arguments{
    \item{object}{an object of class 'cmcq.pepq.masschroq', 'cmcq.pepq.skyline' or 'cmcq.pepq.by.track'}
    \item{file}{a character string indicating the name of the pdf file where to export the graphs (e.g. "mygraph.pdf"). By default, the graphs are displayed on the screen} 
}

\details{'mcq.plot.peptide.reproducibility' draws two graphs: a histogram of distribution of the number of injections in which a peptide-mz is quantified and a plot showing the number peptides-mz that are lost depending on the percentage of missing values tolerated per peptide-mz.}

\value{'mcq.plot.peptide.reproducibility' returns two graphs that are displayed on the screen when the pdf argument is NULL or that are exported as a pdf file when the pdf argument is filled in.}

\references{}

\author{}

\note{}


\seealso{}

\examples{
# Preliminary step: load the example dataset 
	data(strepto)

# Launch 'mcq.plot.peptide.reproducibility'
	mcq.plot.peptide.reproducibility(xicraw)
}
 
