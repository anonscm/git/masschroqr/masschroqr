\name{mcq.compute.ratio}

\alias{mcq.compute.ratio}

\title{Computation of Protein Abundance Ratios}

\description{This function computes the abundance ratios between two levels of a factor of interest}

\usage{mcq.compute.ratio(object, flist=NULL, numerator=NULL, denominator=NULL)}

\arguments{
	\item{object}{an object of class 'cmcq.protq.xic'}
	\item{flist}{a character vector indicating the factor or the combination of factors of interest}
	\item{numerator}{a character string indicating the level of the factor of interest to be used as numerator}
	\item{denominator}{a character string indicating the level of the factor of interest to be used as denominator}
}

\details{}

\value{'mcq.compute.ratio' returns an object of class 'cmcq.ratio'.}

\references{}

\author{}

\note{}

\seealso{}

\examples{
# Preliminary step: load the example dataset
	data("strepto")
	
# Compute the abundance ratio between two levels
	ratio <- mcq.compute.ratio(xicab,flist="strain",numerator="Coel",denominator="Livi")

}

