\name{mcq.plot.peak.width}

\alias{mcq.plot.peak.width}

\title{Plotting of Peak Widths}

\description{This function draws a histogram of distribution of the chromatographic peak widths.}

\usage{mcq.plot.peak.width(object, file=NULL, limit=NULL, nclass=40)}

\arguments{
	\item{object}{an object of class 'cmcq.pepq.masschroq' or 'cmcq.pepq.sds'}
	\item{file}{a character vector indicating the name of the pdf file where to export the graph (e.g. "mygraph.pdf"). By default, the graph is displayed on the screen}
	\item{limit}{a numerical value indicating the upper bound of the x-axis}
	\item{nclass}{an integer indicating the number of bars to be drawn}
}

\details{Peak widths are computed from the retention times at the beginning and the end of the chromatographic peaks (referred to as 'rtbegin' and 'rtend' in the peptides slot of the object of class 'mcqProtPep', respectively).}

\value{'mcq.plot.peak.width' returns a graph that is displayed on the screen when the file argument is NULL or that is exported as a pdf file when the file argument is filled in.}

\references{}

\author{}

\note{}

\seealso{'hist'}

\examples{
# Preliminary step: load the example dataset
	data(strepto)

# Launch 'mcq.plot.peak.width'
	mcq.plot.peak.width(xicraw)

# Zoom in the narrow peak region (0<width<20) 
	mcq.plot.peak.width(xicraw, limit=20)
	mcq.plot.peak.width(xicraw, limit=20, nclass=200)
}


