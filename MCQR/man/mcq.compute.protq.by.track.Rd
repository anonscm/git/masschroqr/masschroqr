\name{mcq.compute.protq.by.track}

\alias{mcq.compute.protq.by.track}

\title{Aggregation of protein counts by track}

\description{This function sums the counts from all fractions from one track for each protein.}

\usage{mcq.compute.protq.by.track(object)}

\arguments{
	\item{object}{an object of class 'cmcq.protq.sds'}
}

\details{}

\value{'mcq.compute.protq.by.track' function returns an object of class 'cmcq.protq.by.track'.}

\references{}

\author{}

\note{}

\seealso{}

\examples{

}

