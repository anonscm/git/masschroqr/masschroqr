\name{mcq.compute.peptide.imputation}

\alias{mcq.compute.peptide.imputation}

\title{Imputation of Peptide Intensities}

\description{This function replaces missing peptide intensities by a value obtained by using an iterative robust model-based imputation.}

\usage{mcq.compute.peptide.imputation(object)}

\arguments{
  \item{object}{an object of class 'cmcq.pepq.masschroq', 'cmcq.pepq.skyline' or 'cmcq.pepq.by.track'}
}

\details{}

\value{'mcq.compute.peptide.imputation' returns an object of the same class as the input object}

\references{}

\author{}

\note{'mcq.compute.peptide.imputation' calls 'irmi' in the 'VIM' package}

\seealso{'irmi' in the 'VIM' package}

\examples{
# Preliminary step: load the example dataset, merge metadata to the object of class 'cmcq.pepq.masschroq' and normalize peptide intensities
	data("strepto")
	xicraw <- mcq.merge.metadata(xicraw, meta)
	xicraw <- mcq.compute.normalization(xicraw)
	xic <- mcq.drop.shared.peptides(xicraw)

# Impute missing intensity data
	xic <-mcq.compute.peptide.imputation(xic)

}

