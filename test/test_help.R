library('MassChroqR')
packageVersion("MassChroqR")
cat(paste(Sys.time(),"\n",sep =""))
setwd('/tmp')

# .Rd used
help(summary)
help(mcq.read.masschroq)
help(mcq.plot.protein.abundance)
help(mcq.plot.counts.per.injection)
help(mcq.read.metadata)
help(mcq.merge.metadata)
help(mcq.compute.pca)
help(mcq.plot.pca)
help(mcq.compute.normalization)
help(mcq.compute.peptide.imputation)
help(mcq.protquantity)
# .Rd existing, used ?
#help(mcqCutoffRetentionTimeSd)
#help(MassChroqR-package)
#help(read.mcq)
#help(mcqWriteMetadataCsv)
#help(mcqWriteCsv)
#help(mcqSelectRepeatablePeptides)
#help(mcqProtPep-class)
#help(mcqPlotRetentionTimeSd)
#help(mcqPlotPeptideRepeatability)
#help(mcqPlotPeakWidth.Rd
#help(mcqPlotNormalizationEffect.Rd
#help(mcqPlotDistribQlog.Rd
#help(mcqPlotDistribQlogDiffRef.Rd
#help(mcqPlotDistribQlogCometDiff.Rd
#help(mcqPeakcountingWriteCsv.Rd
#help(mcqPeakCounting.Rd
#help(mcqPeakcountingPlotPeakPerSamples.Rd
#help(mcqPeakcounting-class.Rd
#help(mcqPeakcountingAnovaVisuSignif.Rd
#help(mcqPeakcountingAnova.Rd
#help(mcqDropRetentionTimeSd.Rd
#help(mcqDropPeakWidth.Rd

# .Rd to create
