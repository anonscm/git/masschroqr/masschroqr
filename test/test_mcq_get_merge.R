library("MassChroqR")
packageVersion("MassChroqR")

cat(paste(Sys.time(),"\n",sep =""))

local_tests=TRUE

source ("./data_sample_for_tests_v1.R")
source("./data_sample_for_tests_sds.R")

dir.create("/tmp/test_MassChroqR", showWarnings = FALSE)

dir.create("/tmp/test_MassChroqR/test_mcq_get_merge", showWarnings = FALSE)

setwd ("/tmp/test_MassChroqR/test_mcq_get_merge")

cat("Test get merge\n")
cat(" ")   

if (file.exists("protpep.RData")) {
	load("protpep.RData")
} else {
	protPep <- mcq.read.masschroq(protFile=mcq_protein_file, pepFile=mcq_peptide_file)
	
	# import des métadata
	metadata <- mcq.read.metadata(mcq_metadata_file)

	# Ajout des metadata a l'objet protPep
	protPep <- mcq.merge.metadata(protPep, metadata)
	save(protPep, file="protpep.RData")
}

cat("\n")
cat("#####################################\n")
cat("      test bad object protPep 1 : mcq.get.merge (protPep) \n")
cat("#####################################\n")
cat("error test 1 : \n")
result <- tryCatch({

		df <- mcq.get.merge(protPep)
			
		}, warning = function(war) {

			# warning handler picks up where error was generated
			cat(paste("MY_WARNING:  ",war))
			return()

		}, error = function(err) {

			# error handler picks up where error was generated
			cat(paste("MY_ERROR:  ",err))

			cat("#####################################\n")
			cat (" error message expected : the class \"cmcq.pepq.masschroq\" is not taken into account by mcq.get.merge.\n")
			cat("#####################################\n")

			return()

		})

cat("\n")
cat("#####################################\n")
cat("      test pkcount 2 : mcq.get.merge (pkcount,pkcount.anova) \n")
cat("#####################################\n")
pkcount <- mcq.compute.peak.counting(protPep)
flist="extraction"
pkcount.anova <- mcq.compute.anova(pkcount, flist=flist)
df <- mcq.get.merge(pkcount,pkcount.anova)
cat("\n")
cat("#####################################\n")
cat("      nb prot df = nrow(pkcount@proteins)  ? \n")
cat("#####################################\n")
dim(df)[1]==nrow(pkcount@proteins)
cat("\n")
cat("#####################################\n")
cat("      nb injections = 19  ? \n")
cat("#####################################\n")
length(grep("samp",colnames(df)))==19
cat("\n")
cat("#####################################\n")
cat("      nb colonnes de df moins la première (accession) - les samp - la colonne protein = nb de colonnes dans resultanov ? \n")
cat("#####################################\n")
length(colnames(df))-1-length(grep("samp",colnames(df)))-1==length(pkcount.anova@resultanov)
cat("\n")
cat("#####################################\n")
cat("      head df \n")
cat("#####################################\n")
head(df)


cat("\n")
cat("#####################################\n")
cat("      test qprot 1 : mcq.get.merge (qprot,qprot.anova) \n")
cat("#####################################\n")
qprot <- mcq.compute.protein.abundances(protPep)
qprot.anova <- mcq.compute.anova(qprot, 'extraction')
df <- mcq.get.merge(qprot,qprot.anova)
cat("\n")
cat("#####################################\n")
cat("      nb prot df = nrow(pkcount@proteins)  ? \n")
cat("#####################################\n")
dim(df)[1]==nrow(qprot@proteins)
cat("\n")
cat("#####################################\n")
cat("      nb injections = 19  ? \n")
cat("#####################################\n")
length(grep("samp",colnames(df)))==19
cat("\n")
cat("#####################################\n")
cat("      nb colonnes de df moins la première (accession) - les samp - la colonne protein = nb de colonnes dans resultanov ? \n")
cat("#####################################\n")
length(colnames(df))-1-length(grep("samp",colnames(df))) -1==length(qprot.anova@resultanov)
cat("\n")
cat("#####################################\n")
cat("      head df \n")
cat("#####################################\n")
head(df)

cat("\n")
cat("#####################################\n")
cat("      test spectral count 1 : mcq.get.merge (spectral.count,spectral.count.anova) \n")
cat("#####################################\n")
spectral.count <- mcq.read.spectral.counting(xtp_spectralcount_file)
metadata <- mcq.read.metadata(xtp_spectralcount_metadata_file)
spectral.count <- mcq.merge.metadata(spectral.count, metadata,override=TRUE)
spectral.count.anova <- mcq.compute.anova(spectral.count, "extraction")
df <- mcq.get.merge(spectral.count,spectral.count.anova)
cat("\n")
cat("#####################################\n")
cat("      nb prot df = nrow(pkcount@proteins)  ? \n")
cat("#####################################\n")
dim(df)[1]==nrow(spectral.count@proteins)
cat("\n")
cat("#####################################\n")
cat("      nb injections = 19  ? \n")
cat("#####################################\n")
length(grep("samp",colnames(df)))==19
cat("\n")
cat("#####################################\n")
cat("      nb colonnes de df moins la première (accession) - les samp- la colonne protein = nb de colonnes dans resultanov ? \n")
cat("#####################################\n")
length(colnames(df))-1-length(grep("samp",colnames(df)))-1==length(spectral.count.anova@resultanov)
cat("\n")
cat("#####################################\n")
cat("      head df \n")
cat("#####################################\n")
head(df)

cat("#####################################\n")
cat("      test sds 1 : mcq.get.compar (XIC) \n")
cat("#####################################\n")

XICRAW.SDS <- mcq.read.masschroq.sds(directory=mcq_directory)
META <- mcq.read.metadata(mcq_metadata_sds)
XICRAW.SDS <- mcq.merge.metadata(XICRAW.SDS, metadata=META)

XICRAW.SDS <- mcq.drop(XICRAW.SDS,factor="fraction", levels="B1")
topprot <- mcq.read.protlist(protFile = "protlist_from_removed_fractions.tsv")

XIC.BY.TRACK <-mcq.compute.pepq.by.track(XICRAW.SDS)
XICAB <- mcq.compute.protein.abundances(XIC.BY.TRACK)
XIC.ANOVA <- mcq.compute.anova(XICAB, "mutant")

df <- mcq.get.merge(XICAB,XIC.ANOVA, prot.to.top = topprot)

cat("\n")
cat("#####################################\n")
cat("      nb prot df = nrow(pkcount@proteins)  ? \n")
cat("#####################################\n")
dim(df)[1]==nrow(XICAB@proteins)
cat("\n")
cat("#####################################\n")
cat("      nb injections = 8  ? \n")
cat("#####################################\n")
length(grep("msrun",colnames(df)))==8
cat("\n")
cat("#####################################\n")
cat("      nb colonnes de df moins la première (accession) - les samp- la colonne protein - la col top.to.top = nb de colonnes dans resultanov ? \n")
cat("#####################################\n")
length(colnames(df))-1-length(grep("msrun",colnames(df)))-2==length(XIC.ANOVA@resultanov)
cat("\n")
cat("\n")
cat("#####################################\n")
cat("      nb prot toppees = 102 ? \n")
cat("#####################################\n")
length(which(df$top=="TRUE"))==102
cat("\n")

cat("\n")
cat("#####################################\n")
cat("      head df \n")
cat("#####################################\n")
head(df)

cat("###########################################\n")
cat("                Test get_merge OK                                                             \n ")
cat("###########################################\n")


