if (local_tests) {
	#local tests
	local_path="~/developpement/data_for_masschroqr_test/nov2016_tests_v1/"

	mcq_protein_file=paste(local_path,"XIC_result_prot.tsv",sep="")
	mcq_protein_missing_peptide_file=paste(local_path,"XIC_result_prot_missing_peptide.tsv",sep="")
	mcq_peptide_file=paste(local_path,"XIC_result_pep.tsv",sep="")
	mcq_metadata_file=paste(local_path,"metadata.ods",sep="")
	mcq_metadata_ws_file=paste(local_path,"metadata_whitespace.ods",sep="")
	mcq_metadata_1factor_file=paste(local_path,"metadata_1factor.ods",sep="")
	mcq_metadata_bad_file=paste(local_path,"badmetadata.ods",sep="")

	xtp_spectralcount_file = paste(local_path,"results_compar_spectra.txt",sep="")
	xtp_spectralcount_metadata_file = paste(local_path,"metadata.ods",sep="")

}else{
	#data used for tests :
	mcq_protein_file="/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/nov2016_tests_v1/XIC_result_prot.tsv"
	mcq_protein_missing_peptide_file="/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/nov2016_tests_v1/XIC_result_prot_missing_peptide.tsv"
	mcq_peptide_file="/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/nov2016_tests_v1/XIC_result_pep.tsv"
	mcq_metadata_file="/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/nov2016_tests_v1/metadata.ods"
	mcq_metadata_ws_file="/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/nov2016_tests_v1/metadata_whitespace.ods"
	mcq_metadata_1factor_file="/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/nov2016_tests_v1/metadata_1factor.ods"
	mcq_metadata_bad_file="/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/nov2016_tests_v1/badmetadata.ods"

	xtp_spectralcount_file = "/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/nov2016_tests_v1/results_compar_spectra.txt"
	xtp_spectralcount_metadata_file = "/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/nov2016_tests_v1/metadata.ods"
}
