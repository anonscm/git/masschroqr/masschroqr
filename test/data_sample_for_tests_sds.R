if (local_tests) {
	#local tests
	local_path="~/developpement/data_for_masschroqr_test/sds_beauvallet/"

	mcq_directory=paste(local_path,"result1.d",sep="")
	mcq_metadata_sds=paste(local_path,"metadata.ods",sep="")
	mcq_metadata_sds_msrun_rename=paste(local_path,"metadata_msrun_rename.ods",sep="")
	mcq_metadata_sds_without_track=paste(local_path,"metadata_without_track.ods",sep="")
	xtp_spectralcount_sds_file = paste(local_path,"20180912_spectra.tsv",sep="")
	xtp_spectralcount_sds_metadata_file = paste(local_path,"sds_metadata.ods",sep="")

}else{
	#data used for tests :
	mcq_directory="/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/sds_beauvallet/result1.d"
	mcq_metadata_sds="/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/sds_beauvallet/metadata.ods"
	mcq_metadata_sds_msrun_rename="/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/sds_beauvallet/metadata_msrun_rename.ods"
	mcq_metadata_sds_without_track="/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/sds_beauvallet/metadata_without_track.ods"
	xtp_spectralcount_sds_file="/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/sds_beauvallet/20180912_spectra.tsv"
	xtp_spectralcount_sds_metadata_file="/gorgone/pappso/versions_logiciels_pappso/mcqr/data_for_test/sds_beauvallet/sds_metadata.ods"

}
