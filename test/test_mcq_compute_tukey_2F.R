library("MassChroqR")
packageVersion("MassChroqR")

cat(paste(Sys.time(),"\n",sep =""))

local_tests <- TRUE

source ("./data_sample_for_tests_v3.R")

test_path <- getwd()

dir.create("/tmp/test_MassChroqR", showWarnings = FALSE)

dir.create("/tmp/test_MassChroqR/test_mcq_compute_tukey_2F", showWarnings = FALSE)

setwd ("/tmp/test_MassChroqR/test_mcq_compute_tukey_2F")

cat("Test compute tukey 2F\n")
cat(" ")   

if (file.exists("protpep.RData")) {
	load("protpep.RData")
} else {
	protPep <- mcq.read.masschroq(protFile=mcq_protein_file, pepFile=mcq_peptide_file)
	
	# import des métadata
	metadata <- mcq.read.metadata(mcq_metadata_file)

	# Ajout des metadata a l'objet protPep
	protPep <- mcq.merge.metadata(protPep, metadata)
	save(protPep, file="protpep.RData")
}

cat("\n")
cat("#####################################\n")
cat("      test bad object protPep 1 :  \n")
cat("#####################################\n")
result <- tryCatch({

		mcq.compute.tukey(protPep)
			
		}, warning = function(war) {

			# warning handler picks up where error was generated
			cat(paste("MY_WARNING:  ",war))
			return()

		}, error = function(err) {

			# error handler picks up where error was generated
			cat(paste("MY_ERROR:  ",err))

			cat("#####################################\n")
			cat (" error message expected : the class \"cmcq.pepq.masschroq\" is not taken into account by mcq.compute.tukey.\n")
			cat("#####################################\n")

			return()

		})


cat("\n")
cat("#####################################\n")
cat("      test1 pkcount  \n")
cat("#####################################\n")
pkcount <- mcq.compute.peak.counting(protPep)
flist <- c("jour","zone")
pkcount.anova.2F <- mcq.compute.anova(pkcount, flist=flist)

pkcount.anova.2F.selected <- mcq.select.pvalues(pkcount.anova.2F, padjust=FALSE, alpha=0.05, flist="jour")
length(pkcount.anova.2F.selected@proteins)==43

result <- tryCatch({

		pkcount.2F.tukey <- mcq.compute.tukey(pkcount.anova.2F, flist=flist, protlist=pkcount.anova.2F.selected)
			
		}, warning = function(war) {

			# warning handler picks up where error was generated
			cat(paste("MY_WARNING:  ",war))
			return()

		}, error = function(err) {

			# error handler picks up where error was generated
			cat(paste("MY_ERROR:  ",err))

			cat("#####################################\n")
			cat (" error message expected : Run anova without interaction term to perform tukey test with counting data.\n")
			cat("#####################################\n")

			return()

		})
		


cat("\n")
cat("#####################################\n")
cat("      test2 pkcount  \n")
cat("#####################################\n")
pkcount <- mcq.compute.peak.counting(protPep)
flist <- c("jour","zone")
pkcount.anova.2F <- mcq.compute.anova(pkcount, flist=flist, inter=FALSE)

pkcount.anova.2F.selected <- mcq.select.pvalues(pkcount.anova.2F, padjust=FALSE, alpha=0.05, flist="jour")
length(pkcount.anova.2F.selected@proteins)==43

pkcount.2F.tukey <- mcq.compute.tukey(pkcount.anova.2F, flist="jour", protlist=pkcount.anova.2F.selected)

cat("\n")
cat("#####################################\n")
cat("      plot and write tukey 1F 'jour'\n")
cat("#####################################\n")

result <- tryCatch({

		mcq.plot.tukey(pkcount.anova.2F, protlist=pkcount.anova.2F.selected,qprot=pkcount.anova.2F.selected)
			
		}, warning = function(war) {

			# warning handler picks up where error was generated
			cat(paste("MY_WARNING:  ",war))
			return()

		}, error = function(err) {

			# error handler picks up where error was generated
			cat(paste("MY_ERROR:  ",err))

			cat("#####################################\n")
			cat (" error message expected : the class \"cmcq.anova\" is not taken into account by mcq.plot.Tukey.\n")
			cat("#####################################\n")

			return()

		})
		
result <- tryCatch({

		mcq.plot.tukey(pkcount.2F.tukey, protlist=pkcount.anova.2F.selected,qprot=pkcount.anova.2F.selected)
			
		}, warning = function(war) {

			# warning handler picks up where error was generated
			cat(paste("MY_WARNING:  ",war))
			return()

		}, error = function(err) {

			# error handler picks up where error was generated
			cat(paste("MY_ERROR:  ",err))

			cat("#####################################\n")
			cat (" error message expected : object is not object of class 'cmcq.protq'.\n")
			cat("#####################################\n")

			return()

		})


mcq.plot.tukey(pkcount.2F.tukey,file="pkcount_2F_tukey_jour.pdf",qprot=pkcount)

mcq.write.dataframe(pkcount.2F.tukey,file="pkcount_2F_tukey_jour.tsv")

cat("\n")
cat("#####################################\n")
cat("      test3 pkcount  \n")
cat("#####################################\n")
# modifie pour tester le message
class(pkcount.anova.2F@model[[1]])[1]="ll"

result <- tryCatch({

		pkcount.2F.tukey <- mcq.compute.tukey(pkcount.anova.2F, flist=flist, protlist=pkcount.anova.2F.selected)
			
		}, warning = function(war) {

			# warning handler picks up where error was generated
			cat(paste("MY_WARNING:  ",war))
			return()

		}, error = function(err) {

			# error handler picks up where error was generated
			cat(paste("MY_ERROR:  ",err))

			cat("#####################################\n")
			cat (" error message expected : There is no glm or aov model in your anova object.\n")
			cat("#####################################\n")

			return()

		})

# remet pour poursuivre les tests
class(pkcount.anova.2F@model[[1]])[1]="glm"



cat("\n")
cat("#####################################\n")
cat("      test qprot 2F mais tukey 'jour'\n")
cat("#####################################\n")

qprot <- mcq.compute.protein.abundances(protPep)

flist <- c("jour","zone")

qprot.anova.2F <- mcq.compute.anova(qprot, flist=flist)

qprot.anova.2F.selected <- mcq.select.pvalues(qprot.anova.2F, padjust=FALSE, alpha=0.01, flist="jour")
length(qprot.anova.2F.selected@proteins)==28

qprot.2F.tukey <- mcq.compute.tukey(qprot.anova.2F, flist="jour", protlist=qprot.anova.2F.selected)

cat("\n")
cat("#####################################\n")
cat("      plot and write tukey 2F 'jour'\n")
cat("#####################################\n")
mcq.plot.tukey(qprot.2F.tukey,file="qprot_tukey_2F_jour.pdf", qprot=qprot)

mcq.write.dataframe(qprot.2F.tukey,file="qprot_tukey_2F_jour.tsv")


cat("\n")
cat("#####################################\n")
cat("      test qprot 2F 'c(jour,zone)\n")
cat("#####################################\n")

#qprot.anova.2F <- mcq.compute.anova(qprot, flist=flist)

qprot.anova.2F.selected.interaction <- mcq.select.pvalues(qprot.anova.2F, padjust=FALSE, alpha=0.05, flist="jour__zone")
length(qprot.anova.2F.selected.interaction@proteins)==5

qprot.2F.tukey.interaction <- mcq.compute.tukey(qprot.anova.2F, flist=c("jour","zone"), protlist=qprot.anova.2F.selected.interaction)

cat("\n")
cat("#####################################\n")
cat("      plot and write tukey 2F 'jour_zone' \n")
cat("#####################################\n")
mcq.plot.tukey(qprot.2F.tukey.interaction,file="qprot_tukey_2F_interaction.pdf",qprot=qprot)

mcq.write.dataframe(qprot.2F.tukey.interaction,file="qprot_tukey_2F_interaction.tsv")

cat("\n")
cat("#####################################\n")
cat("      test SC \n")
cat("#####################################\n")

spectral.count <- mcq.read.spectral.counting(xtp_spectralcount_file)
metadata <- mcq.read.metadata(xtp_spectralcount_metadata_file)
spectral.count <- mcq.merge.metadata(spectral.count, metadata,override=TRUE)

flist <- "jour"
sc.anova <- mcq.compute.anova(spectral.count, flist=flist)
sc.anova.selected <- mcq.select.pvalues(sc.anova, padjust=TRUE, alpha=0.01, flist=flist)
sc.anova.tukey <- mcq.compute.tukey(sc.anova, flist=flist, protlist=sc.anova.selected)

cat("\n")
cat("#####################################\n")
cat("      test SC : plot and write tukey 'jour'\n")
cat("#####################################\n")
mcq.plot.tukey(sc.anova.tukey,file="sc_tukey_jour.pdf", qprot=spectral.count)

mcq.write.dataframe(sc.anova.tukey,file="sc_tukey_jour.tsv")

cat("\n")
cat("#####################################\n")
cat("      test SC Florian_Lamouche \n")
cat("#####################################\n")

source (paste(test_path,"/data_sample_for_tests_tukey.R",sep=""))


spectral.count <- mcq.read.spectral.counting(xtp_spectralcount_file)
metadata <- mcq.read.metadata(xtp_spectralcount_metadata_file)
spectral.count <- mcq.merge.metadata(spectral.count, metadata,override=TRUE)
SC <- mcq.drop(spectral.count, factor="msrun", levels=c("mcqsca0","mcqsca7", "mcqscb10", "mcqscb11", "mcqscb14"))
SC <- mcq.drop.low.proteins(SC, cutoff=3) 
SC <-  mcq.drop.low.variations(SC, cutoff=1.5, flist=c("plant_or_culture"))
SC_ANOVA <- mcq.compute.anova(SC, flist=c("condition"))
SC_PROTEINS_selected <- mcq.select.pvalues(SC_ANOVA, padjust=TRUE, alpha=0.05, flist="condition")
SC_selected_tukey <- mcq.compute.tukey(SC_ANOVA, flist="condition", protlist=SC_PROTEINS_selected)

cat("\n")
cat("#####################################\n")
cat("      test SC Florian_Lamouche : plot and write tukey 'condition'\n")
cat("#####################################\n")
#plist <- mcq.protlist(c("NC_004463.377|bll0332|bll0332|","NC_004463.6387|blr5574|blr5574|","NC_004463.8997|blr7922|blr7922|","NC_004463.30|bll0020|bll0020|"))

mcq.plot.tukey(SC_selected_tukey,file="sc_tukey_florian_lamouche_bis.pdf", qprot=SC)

mcq.write.dataframe(SC_selected_tukey,file="sc_tukey_florian_lamouche_bis.tsv")

cat("\n")
cat("#####################################\n")
cat("      test SC Florian_Lamouche : deviance negative\n")
cat("#####################################\n")

testlist <- mcq.grep.protein.list(SC_PROTEINS_selected,"groEL")
SC_ANOVA_test <- mcq.select.from.protlist(SC_ANOVA, testlist)
#SC_ANOVA_test@model[[2]]$deviance
#[1] 0.6697879
SC_ANOVA_test@model[[2]]$deviance <- -7.10542735760213e-15
SC_test_tukey <- mcq.compute.tukey(SC_ANOVA_test, flist="condition",protlist=testlist)

cat("\n")
cat("#####################################\n")
cat("      test SC Florian_Lamouche : plot and write tukey 'condition'\n")
cat("#####################################\n")

mcq.plot.tukey(SC_test_tukey,file="sc_tukey_florian_lamouche_devneg.pdf", qprot=SC)

mcq.write.dataframe(SC_test_tukey,file="sc_tukey_florian_lamouche_devneg.tsv")


cat("###########################################\n")
cat("                Test compute tukey 2F OK                                                             \n ")
cat("###########################################\n")


