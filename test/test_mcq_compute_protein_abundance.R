library("MassChroqR")
packageVersion("MassChroqR")

cat(paste(Sys.time(),"\n",sep =""))

local_tests=TRUE

source ("./data_sample_for_tests_v1.R")

dir.create("/tmp/test_MassChroqR", showWarnings = FALSE)

dir.create("/tmp/test_MassChroqR/test_mcq_compute_protein_abundances", showWarnings = FALSE)

setwd ("/tmp/test_MassChroqR/test_mcq_compute_protein_abundances")

cat("Test mcq_compute_protein_abundances\n")

if (file.exists("protpep.RData")) {
	load("protpep.RData")
} else {
	protPep <- mcq.read.masschroq(protFile=mcq_protein_file, pepFile=mcq_peptide_file)
	
	# import des métadata
	metadata <- mcq.read.metadata(mcq_metadata_file)
	
	# Ajout des métadata à l'objet protPep
	protPep <- mcq.merge.metadata(protPep, metadata)
	save(protPep, file="protpep.RData")
}

cat("\n")
cat("#####################################\n")
cat("      initial summary protPep\n")
cat("#####################################\n")
summary(protPep)
cat("\n")

cat ("*********************************************\n")
cat("  Test mcq imputation data not normalized \n")
cat ("*********************************************\n")

# Computation of the protein abundances as the sum of the peptide intensities
qprot <- mcq.compute.protein.abundances(protPep)

cat("      summary qprot\n")
summary(qprot)
cat("\n")

cat("\n")
cat("############# vérification de la cohérence des proteines\n")
cat("Test: nb de proteins de @proteins - nb de prot de @protval$accession=0?\n")

nrow(qprot@proteins)-length(unique(qprot@protval$accession))==0

cat ("*********************************************\n")
cat("  Test mcq imputation data normalized but not imputated \n")
cat ("*********************************************\n")

protPepnorm <- mcq.compute.normalization(protPep, method="percent")

# Computation of the protein abundances as the sum of the peptide intensities
qprotnorm <- mcq.compute.protein.abundances(protPepnorm)

cat("      summary qprot\n")
summary(qprotnorm)
cat("\n")

cat("\n")
cat("############# vérification de la cohérence des proteines\n")
cat("Test: nb de proteins de @proteins - nb de prot de @protval$accession=0?\n")

nrow(qprotnorm@proteins)-length(unique(qprotnorm@protval$accession))==0

cat ("*********************************************\n")
cat("  Test mcq imputation data imputated \n")
cat ("*********************************************\n")

protPepimpute <- mcq.drop.shared.peptides(protPepnorm)
protPepimpute <- mcq.compute.peptide.imputation(protPepimpute)

# Computation of the protein abundances as the sum of the peptide intensities
qprotimpute <- mcq.compute.protein.abundances(protPepimpute)

cat("      summary qprot\n")
summary(qprotimpute)
cat("\n")

cat("\n")
cat("############# vérification de la cohérence des proteines\n")
cat("Test: nb de proteins de @proteins - nb de prot de @protval$accession=0?\n")

nrow(qprotnorm@proteins)-length(unique(qprotnorm@protval$accession))==0


cat ("*********************************************\n")
cat("  Test param method incorrect\n")
cat ("*********************************************\n")

result <- tryCatch({

		qprot <- mcq.compute.protein.abundances(protPep,method="su")

		}, warning = function(war) {

			# warning handler picks up where error was generated
			print(paste("MY_WARNING:  ",war))
			return()

		}, error = function(err) {

			# error handler picks up where error was generated
			print(paste("MY_ERROR:  ",err))

			cat ("*********************************************\n")
			cat (" error message expected : The method is not taken into account in the 'mcq.compute.protein.abundances' function.\n")
			cat ("*********************************************\n")

			return()

		})
