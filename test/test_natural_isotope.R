library('MassChroqR')
packageVersion("MassChroqR")
cat(paste(Sys.time(),"\n",sep =""))

dir.create('/tmp/test_MassChroqR/test_natural_isotope')

setwd ('/tmp/test_MassChroqR/test_natural_isotope')


write("default method.ni = max " , 'Result_test.txt',append=TRUE)  
#if (file.exists("protpepni.RData")) {
#	load("protpepni.RData")
#} else {
	protPep <- mcq.read.masschroq('/gorgone/pappso/moulon/users/Olivier/20150916_test_mcq_with_ni/XIC_result_prot.tsv', '/gorgone/pappso/moulon/users/Olivier/20150916_test_mcq_with_ni/XIC_result_pep.tsv')

	# import des métadata
	metadata <- mcq.read.metadata('/gorgone/pappso/formation/2014decembre/metadata.ods')

	# Ajout des métadata à l'objet protPep
	protPep <- mcq.merge.metadata(protPep, metadata)
#}

write("summary protPep" , 'Result_test.txt',append=TRUE)   
summary(protPep)

if (length(unique(protPep@peptides$peptiz)) == length(unique(protPep@peptides$peptiniid))) {
	write(" " , 'Result_test.txt',append=TRUE)  
	write("peptiz == peptiniid max test is OK" , 'Result_test.txt',append=TRUE)
} else {
	# redirection des messages vers terminal
	error("peptiz != peptiniid max test is NOT OK")
}

max(protPep@peptides$ninumber)

write(" " , 'Result_test.txt',append=TRUE) 
write("method.ni = mono " , 'Result_test.txt',append=TRUE)  
 
protPep <- mcq.read.masschroq('/gorgone/pappso/moulon/users/Olivier/20150916_test_mcq_with_ni/XIC_result_prot.tsv', '/gorgone/pappso/moulon/users/Olivier/20150916_test_mcq_with_ni/XIC_result_pep.tsv', method.ni="mono")

# import des métadata
metadata <- mcq.read.metadata('/gorgone/pappso/formation/2014decembre/metadata.ods')

# Ajout des métadata à l'objet protPep
protPep <- mcq.merge.metadata(protPep, metadata)

write("summary protPep " , 'Result_test.txt',append=TRUE)   
summary(protPep)
#}

if (length(unique(protPep@peptides$peptiz)) == length(unique(protPep@peptides$peptiniid))) {
	write(" " , 'Result_test.txt',append=TRUE)  
	write("peptiz == peptiniid mono test is OK" , 'Result_test.txt',append=TRUE)
} else {
	error("peptiz != peptiniid mono test is NOT OK")
}


max(protPep@peptides$ninumber)

# redirection des messages vers terminal
cat ("*********************************************\n")
cat (" test natural isotope OK\n")
cat ("*********************************************\n")
