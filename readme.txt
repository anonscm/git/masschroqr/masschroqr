# ************** MassChroqR installation ************

#open an R terminal and execute the following instructions :


## try http:// if https:// URLs are not supported
# if url scheme is not supported :
# options(download.file.method = "curl")

# install bioconductor pachage requirements
source("https://bioconductor.org/biocLite.R")
biocLite("made4")

#install R modules requirements
install.packages(pkgs=c('zoo', 'mvtnorm', 'XML','multcomp','bitops','caTools','gplots','gtools','gdata','chron','stringi','stringr','reshape2','data.table', 'readODS','vcd','lme4','car','robustbase','colorspace','VIM','ade4','clValid','agricolae', 'ggplot2', 'gridExtra', 'Hmisc', 'RColorBrewer', 'plotly', 'plyr', 'dplyr'), repos=c('http://cran.univ-paris1.fr/'))

#temporarily install the unstable R agricolae 1.2.9
install.packages('http://pappso.inra.fr/downloads/masschroqr/agricolae_1.2-9.tar.gz',repos = NULL, type = 'source')

#Finally install the latest version of MassChroqR
install.packages('https://sourcesup.renater.fr/frs/download.php/latestfile/1735/MassChroqR_v0.4.3.R', repos = NULL, type = 'source')

#if this URL is not available, please 
# check instructions on our website :
# http://pappso.inra.fr/bioinfo
